import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LivestreamPage } from './livestream.page';

describe('LivestreamPage', () => {
  let component: LivestreamPage;
  let fixture: ComponentFixture<LivestreamPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LivestreamPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LivestreamPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
